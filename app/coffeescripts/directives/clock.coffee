'use strict'

ZenClock = () ->
  link = (scope, element, attr) ->

    scope.clockFrame = {
      height: 40,
      width: 40,
      cx: 20,
      cy: 20,
      stroke: '#666',
      strokeWidth: 2,
      radius: 15,
      fill: 'white'
    }

    time = scope.time.replace('AM', '').replace('PM','')
    timesplit = time.split(':')
    datetime = new Date '2015', '1', '1', timesplit[0].trim(), timesplit[1].trim(), '0', '0'
    minuteDegree = 6 * datetime.getMinutes()
    hourDegree = 30*(datetime.getHours()%12) + datetime.getMinutes()/2
    scope.hourTransform = "rotate(#{hourDegree} 20 20)"
    scope.minuteTransform = "rotate(#{minuteDegree} 20 20)"
    return


  {
    restrict: 'A',
    templateUrl: 'partials/clockTemplate.html',
    scope: {
      time: '@'
    },
    link: link
  }

angular.module 'MelbourneTrain'
.directive 'zenClock', ZenClock
