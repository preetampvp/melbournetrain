'use strict'

ScheduleController = ($scope, $state, $stateParams, $http, localStorageService) ->
  vm = this
  vm.departures = []
  vm.directionText = ''
  vm.errorMessage = ''
  vm.fetchingData = false
  vm.isFavorite = false

  vm.init = () ->
    vm.line = $stateParams.line
    vm.station = $stateParams.station
    vm.direction = $stateParams.direction
    vm.from = $stateParams.from
    if $stateParams.direction is 'true'
      vm.direction = true
    else
      vm.direction = false

    vm.setDirectionText()
    vm.updateScheduleData()
    vm.setIsFavorite()
    return

  vm.changeDirection = () ->
    vm.setDirectionText()
    vm.updateScheduleData()
    vm.setIsFavorite()
    return

  vm.setDirectionText = () ->
    if vm.direction is true
      vm.directionText = 'Towards city'
    else
      vm.directionText = 'Away from city'
    return

  vm.updateScheduleData = () ->
    vm.errorMessage = ''
    vm.departures = []
    vm.fetchingData = true
    direction = if vm.direction is true then 1 else 0

    $http.post 'https://melbournetrainsapi.herokuapp.com/schedule', {
      station: vm.station,
      line: vm.line,
      direction: direction
    }
    .success (data) ->
      if data.departures
        vm.departures = data.departures
      else
        vm.errorMessage = 'error fetching schedule.'
        if data.error
          vm.errorMessage = data.error

      vm.fetchingData = false
      return
    .error (err) ->
      vm.departures = []
      console.log err
      vm.errorMessage = 'error fetching schedule.'
      vm.fetchingData = false
      return
    .finally () ->
      $scope.$broadcast 'scroll.refreshComplete'
      return
    return

  vm.goBack = () ->
    if vm.from is 'home'
      $state.go 'network'
    else
      $state.go 'favorite'
    return

  vm.toggleFavorite = () ->
    favorites = localStorageService.getObject 'MelbourneTrainFavs'

    if vm.isFavorite
      _.remove favorites, (favorite) ->
        favorite.line is vm.line and favorite.station is vm.station and favorite.direction is vm.direction
      localStorageService.setObject 'MelbourneTrainFavs', favorites
      vm.isFavorite = false

    else
      favorites.push { line: vm.line, station: vm.station, direction: vm.direction }
      localStorageService.setObject 'MelbourneTrainFavs', favorites
      vm.isFavorite = true

    return

  vm.setIsFavorite = () ->
    vm.isFavorite = false
    favorites = localStorageService.getObject 'MelbourneTrainFavs'
    found = _.where favorites, (favorite) ->
      favorite.line is vm.line and favorite.station is vm.station and favorite.direction is vm.direction

    if found? and found.length > 0
      vm.isFavorite = true
    return

  return

angular.module 'MelbourneTrain'
.controller 'ScheduleController', ['$scope', '$state', '$stateParams', '$http', 'LocalStorageService', ScheduleController]
