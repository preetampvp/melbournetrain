'use strict'

FavoriteController = ($state, localStorageService) ->
  vm = this

  vm.favorites = []
  vm.favoritesTowardsCity = []
  vm.favoritesAwayFromCity = []

  vm.init = () ->
    vm.favorites = localStorageService.getObject 'MelbourneTrainFavs'
    console.log vm.favorites
    if vm.favorites? and vm.favorites.length > 0
      vm.favoritesAwayFromCity = _.where vm.favorites, (favorite) ->
        favorite.direction is false

      vm.favoritesTowardsCity = _.where vm.favorites, (favorite) ->
        favorite.direction is true

    return

  vm.goHome = () ->
    $state.go 'network'
    return

  vm.showSchedule = (fav) ->
    $state.go 'schedule', {
      line: fav.line
      station: fav.station
      direction: fav.direction
      from: 'fav'
    }

  return

angular.module 'MelbourneTrain'
.controller 'FavoriteController', ['$state', 'LocalStorageService', FavoriteController]
