'use strict'

NetworkController = ($state) ->
  vm = this
  vm.searchText = ''
  vm.direction = false
  vm.directionText = 'Away from city'

  vm.changeDirectionText = () ->
    if vm.direction is true
      vm.directionText = 'Towards city'
    else
      vm.directionText = 'Away from city'
    return

  vm.selectStation = (line, station) ->
    $state.go 'schedule', {
      line: line
      station: station
      direction: vm.direction
      from: 'home'
    }
    return

  vm.goFavorite = () ->
    $state.go 'favorite'
    return

  vm.network = {
    'Alamein':['Alamein','Ashburton','Burwood','Hartwell','Willison','Riversdale','Camberwell','Auburn','Glenferrie','Hawthorn','Burnley','East Richmond','Richmond','Flinders Street','Southern Cross','Flagstaff','Melbourne Central','Parliament'],
    'Belgrave':['Belgrave','Tecoma','Upwey','Upper Ferntree Gully','Ferntree Gully','Boronia','Bayswater','Heathmont','Ringwood','Heatherdale','Mitcham','Nunawading','Blackburn','Laburnum','Box Hill','Mont Albert','Surrey Hills','Chatham','Canterbury','East Camberwell','Camberwell','Auburn','Glenferrie','Hawthorn','Burnley','East Richmond','Richmond','Flinders Street','Southern Cross','Flagstaff','Melbourne Central','Parliament'],
    'Craigieburn':['Craigieburn','Roxburgh','Coolaroo','Broadmeadows','Jacana','Glenroy','Oak Park','Pascoe Vale','Strathmore','Glenbervie','Essendon','Moonee Ponds','Ascot Vale','Newmarket','Kensington','North Melbourne','Flagstaff','Melbourne Central','Parliament','Flinders Street','Southern Cross'],
    'Cranbourne':['Cranbourne','Merinda Park','Lynbrook','Dandenong','Yarraman','Noble Park','Sandown Park','Springvale','Westall','Clayton','Huntingdale','Oakleigh','Hughesdale','Murrumbeena','Carnegie','Caulfield','Malvern','Armadale','Toorak','Hawksburn','South Yarra','Richmond','Flinders Street','Southern Cross','Flagstaff','Melbourne Central','Parliament'],
    'Frankston':['Frankston','Kananook','Seaford','Carrum','Bonbeach','Chelsea','Edithvale','Aspendale','Mordialloc','Parkdale','Mentone','Cheltenham','Highett','Moorabbin','Patterson','Bentleigh','McKinnon','Ormond','Glenhuntly','Caulfield','Malvern','Armadale','Toorak','Hawksburn','South Yarra','Richmond','Flinders Street','Southern Cross','Flagstaff','Melbourne Central','Parliament'],
    'Glen Waverley':['Glen Waverley','Syndal','Mount Waverley','Jordanville','Holmesglen','East Malvern','Darling','Glen Iris','Gardiner','Tooronga','Kooyong','Heyington','Burnley','East Richmond','Richmond','Flinders Street','Southern Cross','Flagstaff','Melbourne Central','Parliament'],
    'Hurstbridge':['Hurstbridge','Wattle Glen','Diamond Creek','Eltham','Montmorency','Greensborough','Watsonia','Macleod','Rosanna','Heidelberg','Eaglemont','Ivanhoe','Darebin','Alphington','Fairfield','Dennis','Westgarth','Clifton Hill','Victoria Park','Collingwood','North Richmond','West Richmond','Jolimont-MCG','Parliament','Melbourne Central','Flagstaff','Southern Cross','Flinders Street'],
    'Lilydale':['Lilydale','Mooroolbark','Croydon','Ringwood East','Ringwood','Heatherdale','Mitcham','Nunawading','Blackburn','Laburnum','Box Hill','Mont Albert','Surrey Hills','Chatham','Canterbury','East Camberwell','Camberwell','Auburn','Glenferrie','Hawthorn','Burnley','East Richmond','Richmond','Flinders Street','Southern Cross','Flagstaff','Melbourne Central','Parliament'],
    'Pakenham':['Cardinia Road','Officer','Beaconsfield','Berwick','Narre Warren','Hallam','Dandenong','Yarraman','Noble Park','Sandown Park','Springvale','Westall','Clayton','Huntingdale','Oakleigh','Hughesdale','Murrumbeena','Carnegie','Caulfield','Malvern','Armadale','Toorak','Hawksburn','South Yarra','Richmond','Flinders Street','Southern Cross','Flagstaff','Melbourne Central','Parliament'],
    'Sandringham':['Sandringham','Hampton','Brighton Beach','Middle Brighton','North Brighton','Gardenvale','Elsternwick','Ripponlea','Balaclava','Windsor','Prahran','South Yarra','Richmond','Flinders Street','Southern Cross','Flagstaff','Melbourne Central','Parliament'],
    'South Morang':['South Morang','Epping','Lalor','Thomastown','Keon Park','Ruthven','Reservoir','Regent','Preston','Bell','Thornbury','Croxton','Northcote','Merri','Rushall','Clifton Hill','Victoria Park','Collingwood','North Richmond','West Richmond','Jolimont-MCG','Flinders Street','Southern Cross','Flagstaff','Melbourne Central','Parliament'],
    'Stony Point':['Stony Point','Crib Point','Morradoo','Bittern','Hastings','Tyabb','Somerville','Baxter','Leawarra','Frankston'],
    'Sunbury':['Sunbury','Diggers Rest','Watergardens','Keilor Plains','St Albans','Ginifer','Albion','Sunshine','Tottenham','West Footscray','Middle Footscray','Footscray','South Kensington','North Melbourne','Flagstaff','Melbourne Central','Parliament','Flinders Street','Southern Cross'],
    'Upfield':['Upfield','Gowrie','Fawkner','Merlynston','Batman','Coburg','Moreland','Anstey','Brunswick','Jewell','Royal Park','Flemington Bridge','Macaulay','Melbourne Central','North Melbourne','Flagstaff','Parliament','Flinders Street','Southern Cross'],
    'Werribee':['Werribee','Hoppers Crossing','Williams Landing','Aircraft','Laverton','Westona','Altona','Seaholme','Newport','Spotswood','Yarraville','Seddon','Footscray','South Kensington','North Melbourne','Flagstaff','Melbourne Central','Parliament','Southern Cross','Flinders Street'],
    'Williamstown':['Williamstown','Williamstown Beach','North Williamstown','Newport','Spotswood','Yarraville','Seddon','Footscray','South Kensington','North Melbourne','Flagstaff','Melbourne Central','Parliament','Southern Cross','Flinders Street']
  }

  return




angular.module 'MelbourneTrain'
.controller 'NetworkController', ['$state', NetworkController]
