'use strict'

LocalStorageService = ($window) ->
  return {
  set: (key, value) ->
    $window.localStorage[key] = value
    return

  get: (key, defaultValue) ->
    return $window.localStorage[key] || defaultValue

  setObject: (key, object) ->
    $window.localStorage[key] = JSON.stringify object
    return

  getObject: (key) ->
    return JSON.parse $window.localStorage[key] || '[]'
  }



angular.module 'MelbourneTrain'
.factory 'LocalStorageService', ['$window', LocalStorageService]

