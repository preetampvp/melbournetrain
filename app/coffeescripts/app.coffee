'use strict'

angular.module 'MelbourneTrain', ['ionic', 'config']
.run ($ionicPlatform) ->
  $ionicPlatform.ready () ->
    if window.cordova and window.cordova.plugins.Keyboard
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar true

    if window.StatusBar
      StatusBar.styleDefault()
    return
  return

.config ($stateProvider, $urlRouterProvider) ->

  $stateProvider
  .state 'network', {
    url: '/network',
    templateUrl: 'partials/network.html'
  }
  .state 'favorite', {
    url: '/favorite',
    templateUrl: 'partials/favorite.html'
  }
  .state 'schedule', {
    url: '/schedule/line/:line/station/:station/direction/:direction/from/:from',
    templateUrl: 'partials/schedule.html'
  }

  $urlRouterProvider.otherwise '/network'
  return
