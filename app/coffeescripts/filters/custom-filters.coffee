'use strict'

angular.module 'MelbourneTrain'
.filter 'filterLine', () ->
  return (network, searchKey) ->
    if not searchKey or searchKey.trim() is ''
      return network

    filtered = {}
    searchKey = searchKey.toLowerCase()

    _.each network, (stations, line) ->
      index = _.findIndex stations, (station) ->
        station.toLowerCase().indexOf(searchKey) > -1

      if index > -1
        filtered[line] = stations
      return

    return filtered

.filter 'formatTime', () ->
  return (time) ->
    unless time
      return time

    timeSplit = time.split('.')
    hourPart = parseInt(timeSplit[0])
    minutePart = parseInt(timeSplit[1])
    formattedTime = ''

    if hourPart > 0
      hourText = 'hrs'
      if hourPart is 1
        hourText = 'hr'

      formattedTime = "#{hourPart} #{hourText} "

    minText = 'mins'
    if minutePart is 1
      minText = 'min'

    formattedTime+= "#{minutePart} #{minText}"

    return formattedTime
